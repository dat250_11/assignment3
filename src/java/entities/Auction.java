/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  Auction entity class to save auction in the database.
 */
@Entity
@Table(name="auction")
public class Auction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
     @Temporal(TemporalType.TIMESTAMP)
    private Calendar startDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
   
   private float initPrice;
      
    @NotNull 
    @Size(min = 2, max = 50, message = "Please enter the title.")
    private String title;
   
    @OneToOne
    @JoinColumn(name = "user_fk", referencedColumnName = "username")
    private User user;
    
    @OneToOne
    @JoinColumn(name = "bid_fk", referencedColumnName = "id")
    private Bid bid;

    @OneToOne
    @JoinColumn(name = "item_fk", referencedColumnName = "itemID")
    private Item item;
        
    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
   
    public float getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(float initPrice) {
        this.initPrice = initPrice;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
   
    
    public Auction (){}
  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auction)) {
            return false;
        }
        Auction other = (Auction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
       // final StringBuilder sb = new StringBuilder();
       // sb.append("Auction");
       // sb.append("{ title=").append(title).append(";");
       // sb.append("start date=").append(date).append(";");
        //sb.append("start price=").append(initPrice).append("}");
    //return sb.toString();
        return "entities.Auction[ id=" + id + " ]";
    }
    
    
       public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
/**
    public boolean isStatus_published() {
        return status_published;
    }

    public void setStatus_published(boolean status_published) {
        this.status_published = status_published;
    }
     */
    
    public boolean isActive(Date date){
     return date.after(new Date());
    }
    
   
    
}
