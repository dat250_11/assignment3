/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Auction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import session.AuctionFacade;
import session.UserFacade;

/**
 *
 * @author Julia
 */
@Named(value = "allAuctionsView")
@Dependent
public class allAuctionsView {
    @EJB
    AuctionFacade auctionFacade;
    
    @EJB
    protected UserFacade userFacade;

    
    
//    List<Auction> allAuctions;
//    List<Auction> userAuctions;
//    List<Auction> activeAuctions;
    private Auction auction;

      /**
     * Creates a new instance of allAuctionsView
     */
    public allAuctionsView() {
    }
    
    private String currentUsername(){
        FacesContext context = FacesContext.getCurrentInstance();
        Application application = context.getApplication();
        LoginManager loginManager = application.evaluateExpressionGet(context, "#{loginManager}", LoginManager.class);

        return loginManager.getUsername();
    }
    
    public List<Auction> getAllAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        return allAuctions;
    }
    
    public List<Auction> getActiveAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> activeAuctions = new ArrayList<Auction>();
        for(Auction a : allAuctions){
            if(a.getEndDate().after(new Date()))
                activeAuctions.add(a);
        }
        return activeAuctions;
    }
    
    public List<Auction> getExpiredAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> expiredAuctions = new ArrayList<Auction>();
        for(Auction a : allAuctions){
            if(a.getEndDate().before(new Date()))
                expiredAuctions.add(a);
        }
        return expiredAuctions;
    }
    
    public List<Auction> getUserAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> userAuctions = new ArrayList<Auction>();
        //User user = userFacade.find(username);
        for(Auction a : allAuctions){
            if(a.getUser().getUsername().equals(this.currentUsername()))
                userAuctions.add(a);
        }
        return userAuctions;
    }
    
    public List<Auction> getActiveUserAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> userAuctions = new ArrayList<Auction>();
        //User user = userFacade.find(username);
        for(Auction a : allAuctions){
            if(a.getUser().getUsername().equals(this.currentUsername()) && a.getEndDate().after(new Date()))
                userAuctions.add(a);
        }
        return userAuctions;
    }
    
    public List<Auction> getExpiredUserAuctions(){
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> userAuctions = new ArrayList<Auction>();
        //User user = userFacade.find(username);
        for(Auction a : allAuctions){
            if(a.getUser().getUsername().equals(this.currentUsername()) && a.getEndDate().before(new Date()))
                userAuctions.add(a);
        }
        return userAuctions;
    }
    
      public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
    
    
    
    
    
}
