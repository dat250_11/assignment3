/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

/**
 *
 * @author Julia
 */


import entities.Auction;
import entities.Item;
import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import session.AuctionFacade;
import session.ItemFacade;
import session.UserFacade;

@Named(value = "oneAuctionView")
@Dependent


public class oneAuctionView implements Serializable {
    
@EJB
protected AuctionFacade auctionFacade;

@EJB 
protected UserFacade userFacade;

@EJB
protected ItemFacade itemFacade;

@ManagedProperty("#{param.id}")
private String auctionId;

private Auction auction;

private Item item;


   
    /**
     * Creates a new instance of oneAuctionView
     */
    public oneAuctionView() {
    }
  
    @PostConstruct // this will execute init() after id is injected
    public void init() {

}

    public Auction getAuction() {
        Long id = Long.parseLong(this.getAuctionId());
        auction = this.auctionFacade.find(id);
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction=auction;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    
    public Item getItem() {
       Long id = Long.parseLong(this.getAuctionId());
       this.item = this.itemFacade.find(id);
        return this.item;
    }

    public String getAuctionId() {
       FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
 
        if(params.get("id") != null) {
            return params.get("id");
        }
        else {
            return this.auctionId;
        }
    }
     

  

    public void setItem(Item item) {
        this.item = item;
    }
    
    
 


    
    
    
    
    
   
    
}
