/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.User;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import session.UserFacade;

/**
 *
 * @author tfrol
 */
@Named(value = "loginManager")
@SessionScoped
public class LoginManager implements Serializable {

    @EJB
    protected UserFacade userFacade;
    private String username;
    private String password;
    private boolean loggedIn;
    

    public boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String validateCredentials(){
        String s;
        //userFacade = new UserFacade();
        User user = userFacade.find(username);
        if(userFacade.find(username) != null && userFacade.find(username).getPassword().equals(password)){
            s = "mainLoggedIn";
            loggedIn = true;
//        }
//                (username, password)){
//            loggedIn = true;
//            s = "mainLoggedIn";
        }else{
            s = "login";
        }
        return s;
    }
    
    public String logout(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
       // loggedIn = false;
      //  return "mainLoggedOut";
      return "/loggedOut.xhtml?faces-redirect=true";
    }
    
    public String viewMain(){
        if(loggedIn) return "mainLoggedIn";
        else return "mainLoggedOut";
    }
    
    /**
     * Creates a new instance of LoginManager
     */
    public LoginManager() {
    }
    
}
