/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Auction;
import entities.Item;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import session.AuctionFacade;
import session.ItemFacade;
import session.UserFacade;

/**
 * Managed bean for auctions.
 */
@Named(value="auctionManager")
@RequestScoped

public class AuctionManager implements Serializable {
@EJB
protected AuctionFacade auctionFacade;
@EJB
protected ItemFacade itemFacade;

@EJB
protected UserFacade userFacade;

private Auction auction;

 private String title;
 private float initPrice;
 private String description;
 private Calendar startDate;
 private Date endDate;
 //private String username;
private Item item;

   
   
FacesContext context = FacesContext.getCurrentInstance();
Application application = context.getApplication();
LoginManager loginManager = application.evaluateExpressionGet(context, "#{loginManager}", LoginManager.class);

private String username = loginManager.getUsername();
//@ManagedProperty(value="#{loginManager.userName}")
//private String username;
//String username = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("username");
//public void setLoginManager(LoginManager loginManager) {
//    this.loginManager = loginManager;
//}

//    public LoginManager getLoginManager() {
//        return loginManager;
//    }



//private String category;

/**  
public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    * */

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }
    
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    } 
 
    public UserFacade getUserFacade() {
        return userFacade;
    }

    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
    
    
    public AuctionFacade getAuctionFacade() {
        return auctionFacade;
    }

    public ItemFacade getItemFacade() {
        return itemFacade;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(float initPrice) {
        this.initPrice = initPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
/*
    public List<Auction> getAuctions()
    {
        Connection connect = null;
        String url = "jdbc:postgresql://data2.hib.no:5433/db17_g11";
        String user = "g11";
        String password = "Kt7E4y4";
        String query = "SELECT * FROM Auction";
        PreparedStatement pst = null;
        ResultSet = null;
        List<Auction> ret = new ArrayList<Auction>();
        
        try
        {
            connect = DriverManager.getConnection(url, user, password);
            pst = connect.prepareStatement(query);
            pst.execute();
            rs = pst.getResultSet();
            
            while(rs.next())
            {
                Auction auction = new Auction();
                auction.setId(rs.getLong(1));
                auction.setName(rs.getString(2));
                auction.setInitPrice(rs.getFloat(3));
                ret.add(auction);
            }
            
        } catch(SQLException e)
        {
            e.printStackTrace();
        }
        
        return ret;
    }
*/    
    public String saveAuction() {
        String s = "auctionCreated";

        try {
            Item item = populateItem(auction);
            Auction auction = populateAuction(item);
            getItemFacade().create(item);
            getAuctionFacade().create(auction);
        } catch (EJBException e) {
            s = "error saving the auction";
            /**
             * @SuppressWarnings("ThrowableResultIgnored") Exception cause =
             * e.getCausedByException(); if (cause instanceof
             * ConstraintViolationException) {
             * @SuppressWarnings("ThrowableResultIgnored")
             * ConstraintViolationException cve = (ConstraintViolationException)
             * e.getCausedByException(); for (Iterator<ConstraintViolation<?>>
             * it = cve.getConstraintViolations().iterator(); it.hasNext();) {
             * ConstraintViolation<? extends Object> v = it.next(); s =
             * v.getMessage(); System.err.println("==>>"+v.getMessage()); } } }
             */
        }
        return s;
    }

    private Auction populateAuction(Item item){
        if (auction == null){
        auction = new Auction();
     } 
        auction.setTitle(title);
        auction.setStartDate(Calendar.getInstance());
        auction.setEndDate(endDate);
        auction.setInitPrice(initPrice);
       // item.setCategory(category);
       auction.setItem(item);
       //User user = userFacade.find(loginManager.getUsername());
       auction.setUser(userFacade.find(username));
       //Bid bid = new Bid();
       //bid.setAmount(0);
       //auction.setBid(bid);
        return auction;
    }  
    
    private Item populateItem(Auction auction){
        Item item = new Item();
        item.setDescription(description);
        item.setAuction(auction);
        return item;
    }
         
}


