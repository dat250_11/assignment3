/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import javax.inject.Named;
import entities.Bid;
import javax.enterprise.context.RequestScoped;
import session.BidFacade;
import session.AuctionFacade;
import entities.User;
import entities.Auction;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import session.UserFacade;

/**
 * Managed bean for bids.
 * @author Annar
 */
@Named(value = "bidManager")
@RequestScoped
public class BidManager {

    @EJB
    protected AuctionFacade auctionFacade;
    @EJB
    protected BidFacade bidFacade;
    @EJB
    protected UserFacade userFacade;
    
    @ManagedProperty("#{param.id}")
    private String auctionId;
    
    FacesContext context = FacesContext.getCurrentInstance();
    Application application = context.getApplication();
    LoginManager loginManager = application.evaluateExpressionGet(context, "#{loginManager}", LoginManager.class);
    AuctionView oneAuctionView = application.evaluateExpressionGet(context, "#{AuctionView}", AuctionView.class);
    
    @Resource(mappedName="jms/auctionConnectionFactory")
	private ConnectionFactory connectionFactory;
	
    @Resource(mappedName="jms/auctionTopic")//topic
	private Topic topic;
    
   // private Long id = oneAuctionView.getAuction().getId();
 //   private Auction auction = oneAuctionView.getAuction();
    private String username = loginManager.getUsername();

    private float amount;
    private User buyer;
    //private Date date; //sort by date functionality?
    
    public AuctionFacade getAuctionFacade() {
        return auctionFacade;
    }

    public void setAuctionFacade(AuctionFacade auctionFacade) {
        this.auctionFacade = auctionFacade;
    }

    public BidFacade getBidFacade() {
        return bidFacade;
    }

    public void setBidFacade(BidFacade bidFacade) {
        this.bidFacade = bidFacade;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    /*
    Saves a bid using the id of the auction
    */
    public String saveBid(String id)
    {
        String s = "bidSaved";
        Auction auction = auctionFacade.find(Long.valueOf(id));
        Bid bid = new Bid();
       
        try
        {
            bid.setUser(userFacade.find(username));
            if(auction.getBid() != null){
                if(amount>auction.getBid().getAmount()){
                    bid.setAmount(amount);
                }
                else{
                    String msg = "Error: The bid amount should be greater than the current bid.";
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
                    //FacesCtontext.getCurrentInstance().addMessage(new FacesMessage("Purchase Amount is invalid"));
                }
            }
            else{
                if (amount>auction.getInitPrice()){
                bid.setAmount(amount);
                //auction = auctionFacade.find(Long.valueOf(auctionId));
                }
                else {
                    String msg = "Error: The bid amount should be greater than the initial price.";
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
                    //FacesCtontext.getCurrentInstance().addMessage(new FacesMessage("Purchase Amount is invalid"));
                 }
            }
            bid.setAuction(auction);
            auction.setBid(bid);
        } catch(IllegalArgumentException e)
        {
            s = "auctionNotFound";
        }
   
        try
        {
            bidFacade.create(bid);
            auctionFacade.edit(auction);
          try
		{
			Connection connection = connectionFactory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer messageProducer = session.createProducer(topic);
			MapMessage message = session.createMapMessage();
                        message.setString("username",auction.getUser().getUsername());
                        message.setString("item",auction.getTitle());
                        message.setLong("auctionId", auction.getId());
                        
                }
            catch (JMSException e){
            }   
         
        } catch(EJBException e)
        {
            s = "bidRegisterError";
        }
        
        return s;
    }
    
    public List<Auction> getAuctions()
    {
        return auctionFacade.findAll();
    }
    
    /**
     * Creates a new instance of BidManager
     */
    public BidManager() {
    }
    
     private boolean showBid;

    public boolean isShowBid() {
        return showBid;
    }

    public void setShowBid(boolean showBid) {
       this.showBid = showBid;
    }
    
   
    
    public boolean show(String id){
    Auction auction = auctionFacade.find(Long.valueOf(id));
     if (!auction.isActive(auction.getEndDate()) || !loginManager.getLoggedIn() || loginManager.getUsername().equals(auction.getUser().getUsername())) return false;
        else  return true;
     }
    
    public String bidMessage(String id){
        Auction auction = auctionFacade.find(Long.valueOf(id));
        if(!loginManager.getLoggedIn()) return "Log in to place a bid.";
        else if(loginManager.getUsername().equals(auction.getUser().getUsername())) return "This is one of your auctions.";
        else return "Auction expired";
    }
    
}
