/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.webauction;

import entities.Auction;
import entities.Bid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import session.AuctionFacade;
import session.BidFacade;

/**
 *
 * @author Tord
 */
@WebService(serviceName = "WebAuctionWS")
//@Stateless()
public class WebAuctionWS {

    @EJB protected BidFacade bidFacade;
    
    @EJB protected AuctionFacade auctionFacade;
    
    
    
    /**
     * Returns the currently active auctions.
     */
    @WebMethod(operationName = "getActiveAuctions")
    public List<Auction> getActiveAuctions() {
        
        List<Auction> allAuctions = auctionFacade.findAll();
        ArrayList<Auction> activeAuctions = new ArrayList<Auction>();
        for(Auction a : allAuctions){
            if(a.getEndDate().after(new Date()))
                activeAuctions.add(a);
        }
        return activeAuctions;
    }
    
    

    // What type of message=

    /**
     * Attempts to place the bid and returns a message indicating the result.
     */
    @WebMethod(operationName = "bidForAuction")
    public String bidForAuction(@WebParam(name = "newBid") Bid newBid) {
        // Check that the bid is not null
        if(newBid == null) return "Error: The bid is null.";
        
        String s = "Bid usccesfully saved.";
        Auction auction = auctionFacade.find(Long.valueOf(newBid.getId()));
       
        // Make sure that the bid is large enough.
        if(auction.getBid() != null){
            if(newBid.getAmount()<auction.getBid().getAmount())
            s = "Error: The bid amount should be greater than the current bid.";
        }
        else if (newBid.getAmount()<auction.getInitPrice()){
            String msg = "Error: The bid amount should be greater than the initial price.";
        }
        // Bid is large enough, try to apply the bid to the auction.
        else{
            try{
                auction.setBid(newBid);
            } catch(IllegalArgumentException e){
                s = "The auction was not found";
            }
        }

        // Persist the bid in the database
        try
        {
            bidFacade.create(newBid);
            auctionFacade.edit(auction);
        } catch(EJBException e)
        {
            s = "There was an error bidding on the item.";
        }
        
        return s;
    }
    
}
