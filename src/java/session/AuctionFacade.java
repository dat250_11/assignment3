/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Auction;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 */

@Stateless
public class AuctionFacade extends AbstractFacade<Auction> {
    

        @PersistenceContext(unitName = "WebAuctionPU")
    private EntityManager em;
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuctionFacade() {
        super(Auction.class);
    }
     public boolean isActive (Date date){
            
        Date currentDate = new Date();
        return date.after(currentDate); 
           
    } 
     
     
    
    
    
    
    
     
    
    
    
      
}
