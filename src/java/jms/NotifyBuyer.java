/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 *
 * @author Julia
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/auctionTopic")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class NotifyBuyer implements MessageListener {
    @Resource 
    private MessageDrivenContext mdc;
    
    public NotifyBuyer() {
    }
    
    @Override
    public void onMessage(Message message) {
    try{
        if (message instanceof MapMessage) {
            //msg = (ObjectMessage) message;
            MapMessage msg = (MapMessage) message;
            
           //message.getBody(String.class);
            
            String link = "http://localhost:8080/assignment2/faces/auctionDescriptionView.xhtml?id=";
           
            
 /**           
            ---- START EMAIL to customer X ----
Dear X,
Congratulations! You have won in bidding for product Y.
You can access the product using the following link:
URL=<LINK>
---- END EMAIL to customer X ----
   */
             System.out.format("---- START EMAIL to customer %s ----%n", msg.getString("username"));
             System.out.format("Dear %s%n", msg.getString("username"));
             System.out.format("Congratilations! You have won in bidding for product %s%n", msg.getString("item"));
             System.out.println("You can access the product using the following link:%n");
             System.out.format("URL=<%s%d>%n",link, msg.getLong("auctionId"));
             System.out.format("---- END EMAIL to customer %s ----%n", msg.getString("username"));      
        }
    } catch (JMSException e) {
        e.printStackTrace();
        mdc.setRollbackOnly();
    
}
    
    
}
}
