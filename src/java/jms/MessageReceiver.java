/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms;

import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;

/**
 *
 * Class for the message subscriber
 */
public class MessageReceiver {
        
    public static void main (String [] args){
    
    
    try {
    InitialContext ctx = new InitialContext();
    Topic topic = (Topic) ctx.lookup("jms/auctionTopic");
    TopicConnectionFactory cf = (TopicConnectionFactory)ctx.lookup("jms/auctionConnectionFactory");
    //Connection connection = cf.createConnection();
    TopicConnection tc = cf.createTopicConnection();
   // Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    TopicSession session = tc.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
    //TopicSubscriber subscriber = session.createDurableSubscriber(topic,"");
    TopicSubscriber subscriber = session.createSubscriber(topic);
    //NotifyBuyer topicListener = new NotifyBuyer();
    subscriber.setMessageListener(new NotifyBuyer());
    tc.start();

    }
    catch(Exception e){
    System.out.println(e);
}
    
}
}
